from lark import Lark, Transformer

from abstract_syntax_tree import Equals, Contains, AtomToSentence, Not, Or, ForAll, Term, Sentence

_parser = Lark(
    r"""
    TERM: /x`*/
    atom: "(" TERM " = " TERM ")"         -> equals
        | "(" TERM " ∈ " TERM ")"         -> contains
    sentence: atom                        -> atom_to_sentence
        | "¬" sentence                    -> not_
        | "(" sentence " ∧ " sentence ")" -> or_
        | "∀" TERM " " sentence           -> for_all
    """,
    start='sentence'
)


class _Transformer(Transformer):

    TERM = Term
    equals = lambda self, args: Equals(*args)
    contains = lambda self, args: Contains(*args)
    atom_to_sentence = lambda self, args: AtomToSentence(*args)
    not_ = lambda self, args: Not(*args)
    or_ = lambda self, args: Or(*args)
    for_all = lambda self, args: ForAll(*args)


def parse_sentence(sentence: str) -> Sentence:
    r"""Returns an abstract syntax tree from a valid sentence.

    Uses the following grammar:

        TERM ::= /x`*/ # (regular expression)
        Atom ::=
            TERM = TERM |
            TERM ∈ TERM
        Sentence ::=
            Atom |
            (¬Sentence) |
            (Sentence ∧ Sentence) |
            (∀TERM Sentence)

    Example:

        >>> tree = parse_sentence("∀x (x = x)")
        >>> tree
        ForAll(Term('x'), AtomToSentence(Equals(Term('x'), Term('x'))))
        >>> print(tree)
        ∀x x = x

    Args:
        sentence: A sentence to parse.
    """
    tree = _parser.parse(sentence)
    return _Transformer().transform(tree)


if __name__ == "__main__":
    import doctest
    doctest.testmod()
