import re
from abc import ABC, abstractmethod

from lark import Token


class Node(ABC):

    @abstractmethod
    def __str__(self) -> str:
        ...


class Term(Node):

    def __init__(self, token: Token):
        name = token.value
        assert re.fullmatch("x`*", name)
        self._name = name

    @property
    def name(self) -> str:
        return self._name

    def __str__(self) -> str:
        return self.name

    def __repr__(self) -> str:
        return f"Term({repr(self.name)})"


class Atom(Node, ABC):
    ...


class Equals(Atom):

    def __init__(self, term1: Term, term2: Term):
        self._term1 = term1
        self._term2 = term2

    @property
    def term1(self) -> Term:
        return self._term1

    @property
    def term2(self) -> Term:
        return self._term2

    def __str__(self) -> str:
        return f"{self.term1} = {self.term2}"

    def __repr__(self) -> str:
        return f"Equals({repr(self.term1)}, {repr(self.term2)})"


class Contains(Atom):

    def __init__(self, term1: Term, term2: Term):
        self._term1 = term1
        self._term2 = term2

    @property
    def term1(self) -> Term:
        return self._term1

    @property
    def term2(self) -> Term:
        return self._term2

    def __str__(self) -> str:
        return f"{self.term1} ∈ {self.term2}"

    def __repr__(self) -> str:
        return f"Contains({repr(self.term1)}, {repr(self.term2)})"


class Sentence(Node, ABC):
    ...


class AtomToSentence(Sentence):

    def __init__(self, atom: Atom):
        self._atom = atom

    @property
    def atom(self) -> Atom:
        return self._atom

    def __str__(self) -> str:
        return str(self.atom)

    def __repr__(self) -> str:
        return f"AtomToSentence({repr(self.atom)})"


class Not(Sentence):

    def __init__(self, sentence: Sentence):
        self._sentence = sentence

    @property
    def sentence(self) -> Sentence:
        return self._sentence

    def __str__(self) -> str:
        return f"(¬{self.sentence})"

    def __repr__(self) -> str:
        return f"Not({repr(self.sentence)})"


class Or(Sentence):

    def __init__(self, sentence1: Sentence, sentence2: Sentence):
        self._sentence1 = sentence1
        self._sentence2 = sentence2

    @property
    def sentence1(self) -> Sentence:
        return self._sentence1

    @property
    def sentence2(self) -> Sentence:
        return self._sentence2

    def __str__(self) -> str:
        return f"({self.sentence1} ^ {self.sentence2})"

    def __repr__(self) -> str:
        return f"Or({repr(self.sentence1)}, {repr(self.sentence2)})"


class ForAll(Sentence):

    def __init__(self, term: Term, sentence: Sentence):
        self._term = term
        self._sentence = sentence

    @property
    def term(self) -> Term:
        return self._term

    @property
    def sentence(self) -> Sentence:
        return self._sentence

    def __str__(self) -> str:
        return f"∀{self.term} {self.sentence}"

    def __repr__(self) -> str:
        return f"ForAll({repr(self.term)}, {repr(self.sentence)})"
